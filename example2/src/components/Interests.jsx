import React from 'react';

const styles = {
  container: {
    marginTop: '-25px',
  },
  h1:{
    width: '20px',
    height: '21px',
    left: '0px',
    top: '0px',
    
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '16px',
    lineHeight: '150%',
    /* or 24px */
    
    
    color: '#2B283A',
  },
  h6: {
    marginTop: '-10px',
    width: '247px',
    height: '61px',
    left: '151px',
    top: '554px',

    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '10.24px',
    lineHeight: '150%',
    /* or 15px */


    color: '#4A4E74',
  }
}

export default function Interests() {
  return (
    <div style={styles.container}>
      <h1 style={styles.h1}>Interests</h1>
      <h6 style={styles.h6}>Food expert. Music scholar. Reader. Internet fanatic. Bacon buff. Entrepreneur. Travel geek. Pop culture ninja. Coffee fanatic.</h6>

    </div>);
}

