import React from 'react';
import foto from '../images/Foto.png'
import { AiFillMail, AiFillLinkedin, AiFillTwitterSquare, AiFillFacebook, AiFillInstagram, AiOutlineGithub } from "react-icons/ai";
import About from './About';
import Interests from './Interests';


const styles = {
    nav: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        alignItem: 'center',

        width: '317px',
        height: '780px',
        left: '116px',
        top: '44px',
        
        background: '#F5F5F5',
    },
    foto: {
       
        width: '317px',
        height: '317px',
        left: '116px',
        top: '44px',
    },
    h1: {
        height: '0px',
        left: '186px',
        top: '0px',

        fontFamily: 'Inter',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: '25px',
        lineHeight: '30px',
      
        color: '#2B283A',
    },
    h2: {  
        height: '14.19px',
        left: '186px',
        top: '413px',

        fontFamily: 'Inter',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: '12.8px',
        lineHeight: '15px',
        
        color: '#F47D27',
    },
    h6: {
        height: '16px',
        left: '161px',
        top: '436px',
        margin: '0',

        fontFamily: 'Inter',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: '10.24px',
        lineHeight: '150%',

        color: '#4A4E74',
    },
    email: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '9px 13px 9px 11px',

        position: 'absolute',
        width: '115px',
        height: '34px',
        left: '151px',
        top: '469px',

        /* white */

        background: '#FFFFFF',
        /* gray/300 */

        border: '1px solid #D1D5DB',
        boxSizing: 'border-box',
        /* shadow/sm */

        boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.05)',
        borderRadius: '6px',
    },
    LinkedIn:{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '9px 13px 9px 11px',

        position: 'absolute',
        width: '115px',
        height: '34px',
        left: '283px',
        top: '469px',

        background: '#297FE5',
        /* gray/300 */

        border: '1px solid #D1D5DB',
        boxSizing: 'border-box',
        /* shadow/sm */

        boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.05)',
        borderRadius: '6px'
    },
    pLinkedIn: {
        color: '#FFFFFF',
    }, 
}



export default function Info() {
  return (  
    <nav style={styles.nav}>
        <img src={foto} alt='fotografia' style={styles.foto}/>
        <h1 style={styles.h1}>Laura Smith</h1>
        <h2 style={styles.h2}>Frontend Developer</h2>
        <h6 style={styles.h6}>laurasmith.website</h6>

        <div>
            <button style={styles.email}> <AiFillMail/> Email</button>
            <button style={styles.LinkedIn}> <AiFillLinkedin style={{color: '#FFFFFF'}}/> <p style={styles.pLinkedIn}>LinkedIn</p> </button>
        </div>

        <About/>
        <Interests/>

        <div>
            <AiFillTwitterSquare/>
            <AiFillFacebook/>
            <AiFillInstagram/>
            <AiOutlineGithub/>
        </div>


     

        
    </nav>
    )
  
  ;
}
