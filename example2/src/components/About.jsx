import React from 'react';

const styles = {
  container: {
    marginTop: '80px',
  },
  h1:{
    width: '20px',
    height: '21px',
    left: '0px',
    top: '0px',
    
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '16px',
    lineHeight: '150%',
    /* or 24px */
    
    
    color: '#2B283A',
  },
  h6: {
    marginTop: '-10px',
    width: '247px',
    height: '61px',
    left: '151px',
    top: '554px',

    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '10.24px',
    lineHeight: '150%',
    /* or 15px */


    color: '#4A4E74',
  }
}

export default function About() {
  return (
    <div style={styles.container}>
      <h1 style={styles.h1}>About</h1>
      <h6 style={styles.h6}>I am a frontend developer with a particular interest in making things simple and automating daily tasks. I try to keep up with security and best practices, and am always looking for new things to learn.</h6>

    </div>);
}
